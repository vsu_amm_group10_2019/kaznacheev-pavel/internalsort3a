﻿
namespace InternalSort
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonForSort = new System.Windows.Forms.Button();
            this.groupBoxForRandomElement = new System.Windows.Forms.GroupBox();
            this.numericUpDownMaxValue = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMinValue = new System.Windows.Forms.NumericUpDown();
            this.labelForMaxValue = new System.Windows.Forms.Label();
            this.labelForMinValue = new System.Windows.Forms.Label();
            this.numericUpDownNumberElemnts = new System.Windows.Forms.NumericUpDown();
            this.labelForNumberElements = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.groupBoxForRandomElement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberElemnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonForSort
            // 
            this.buttonForSort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonForSort.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonForSort.Location = new System.Drawing.Point(587, 31);
            this.buttonForSort.Name = "buttonForSort";
            this.buttonForSort.Size = new System.Drawing.Size(191, 64);
            this.buttonForSort.TabIndex = 0;
            this.buttonForSort.Text = "Отсортировать";
            this.buttonForSort.UseVisualStyleBackColor = true;
            this.buttonForSort.Click += new System.EventHandler(this.buttonForSort_Click);
            // 
            // groupBoxForRandomElement
            // 
            this.groupBoxForRandomElement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxForRandomElement.Controls.Add(this.numericUpDownMaxValue);
            this.groupBoxForRandomElement.Controls.Add(this.buttonForSort);
            this.groupBoxForRandomElement.Controls.Add(this.numericUpDownMinValue);
            this.groupBoxForRandomElement.Controls.Add(this.labelForMaxValue);
            this.groupBoxForRandomElement.Controls.Add(this.labelForMinValue);
            this.groupBoxForRandomElement.Controls.Add(this.numericUpDownNumberElemnts);
            this.groupBoxForRandomElement.Controls.Add(this.labelForNumberElements);
            this.groupBoxForRandomElement.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxForRandomElement.Location = new System.Drawing.Point(12, 59);
            this.groupBoxForRandomElement.Name = "groupBoxForRandomElement";
            this.groupBoxForRandomElement.Size = new System.Drawing.Size(784, 103);
            this.groupBoxForRandomElement.TabIndex = 1;
            this.groupBoxForRandomElement.TabStop = false;
            this.groupBoxForRandomElement.Text = "Генерация";
            // 
            // numericUpDownMaxValue
            // 
            this.numericUpDownMaxValue.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownMaxValue.Location = new System.Drawing.Point(402, 61);
            this.numericUpDownMaxValue.Name = "numericUpDownMaxValue";
            this.numericUpDownMaxValue.Size = new System.Drawing.Size(157, 23);
            this.numericUpDownMaxValue.TabIndex = 6;
            this.numericUpDownMaxValue.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // numericUpDownMinValue
            // 
            this.numericUpDownMinValue.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownMinValue.Location = new System.Drawing.Point(206, 61);
            this.numericUpDownMinValue.Name = "numericUpDownMinValue";
            this.numericUpDownMinValue.Size = new System.Drawing.Size(157, 23);
            this.numericUpDownMinValue.TabIndex = 5;
            // 
            // labelForMaxValue
            // 
            this.labelForMaxValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelForMaxValue.AutoSize = true;
            this.labelForMaxValue.Location = new System.Drawing.Point(399, 31);
            this.labelForMaxValue.Name = "labelForMaxValue";
            this.labelForMaxValue.Size = new System.Drawing.Size(172, 17);
            this.labelForMaxValue.TabIndex = 4;
            this.labelForMaxValue.Text = "Максимальное значение";
            // 
            // labelForMinValue
            // 
            this.labelForMinValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelForMinValue.AutoSize = true;
            this.labelForMinValue.Location = new System.Drawing.Point(203, 31);
            this.labelForMinValue.Name = "labelForMinValue";
            this.labelForMinValue.Size = new System.Drawing.Size(166, 17);
            this.labelForMinValue.TabIndex = 3;
            this.labelForMinValue.Text = "Минимальное значение";
            // 
            // numericUpDownNumberElemnts
            // 
            this.numericUpDownNumberElemnts.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numericUpDownNumberElemnts.Location = new System.Drawing.Point(9, 61);
            this.numericUpDownNumberElemnts.Name = "numericUpDownNumberElemnts";
            this.numericUpDownNumberElemnts.Size = new System.Drawing.Size(157, 23);
            this.numericUpDownNumberElemnts.TabIndex = 2;
            this.numericUpDownNumberElemnts.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // labelForNumberElements
            // 
            this.labelForNumberElements.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelForNumberElements.AutoSize = true;
            this.labelForNumberElements.Location = new System.Drawing.Point(6, 31);
            this.labelForNumberElements.Name = "labelForNumberElements";
            this.labelForNumberElements.Size = new System.Drawing.Size(160, 17);
            this.labelForNumberElements.TabIndex = 2;
            this.labelForNumberElements.Text = "Количество элементов";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.ColumnHeadersVisible = false;
            this.dataGridView.Location = new System.Drawing.Point(12, 12);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView.RowTemplate.Height = 41;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView.Size = new System.Drawing.Size(784, 41);
            this.dataGridView.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(808, 169);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.groupBoxForRandomElement);
            this.MaximumSize = new System.Drawing.Size(1920, 208);
            this.MinimumSize = new System.Drawing.Size(824, 208);
            this.Name = "MainForm";
            this.Text = "InternalForm";
            this.groupBoxForRandomElement.ResumeLayout(false);
            this.groupBoxForRandomElement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberElemnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonForSort;
        private System.Windows.Forms.GroupBox groupBoxForRandomElement;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxValue;
        private System.Windows.Forms.NumericUpDown numericUpDownMinValue;
        private System.Windows.Forms.Label labelForMaxValue;
        private System.Windows.Forms.Label labelForMinValue;
        private System.Windows.Forms.NumericUpDown numericUpDownNumberElemnts;
        private System.Windows.Forms.Label labelForNumberElements;
        public System.Windows.Forms.DataGridView dataGridView;
    }
}

