﻿using System;
using System.Windows.Forms;

namespace InternalSort
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonForSort_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int size = (int)numericUpDownNumberElemnts.Value;
            Record[] array = new Record[size];
            for (int counter = 0; counter < size; counter++)
            {
                array[counter] = new Record();
                array[counter].key = random.Next((int)numericUpDownMinValue.Value, (int)numericUpDownMaxValue.Value);
                array[counter].Number = random.Next((int)numericUpDownMinValue.Value, (int)numericUpDownMaxValue.Value);
            }
            ArrayToGrid(array);
            Sort(array);
            MessageBox.Show("Сортировка выполнена");
        }

        private void ArrayToGrid(Record[] array)
        {
            dataGridView.RowCount = 1;
            dataGridView.ColumnCount = array.Length;
            dataGridView.ClearSelection();
            for (int i = 0; i < array.Length; i++)
            {
                dataGridView.Rows[0].Cells[i].Value = array[i].key;
            }
            dataGridView.Refresh();
            Delay(1000);
        }

        private void Swap(Record[] array, int i, int j)
        {
            Record tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
            ArrayToGrid(array);
            dataGridView.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.White;
            dataGridView.Rows[0].Cells[j].Style.BackColor = System.Drawing.Color.White;
        }

        private Record[] Sort(Record[] array)
        {
            int size = array.Length;
            for (int i = size / 2 - 1; i >= 0; i--)
            {
                ArrayToHeap(array, i, size);
            }
            for (int i = size - 1; i >= 0; i--)
            {
                dataGridView.Rows[0].Cells[0].Style.BackColor = System.Drawing.Color.Red;
                dataGridView.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.Green;
                dataGridView.Refresh();
                Delay(1000);
                Swap(array, 0, i);
                ArrayToHeap(array, 0, i);
            }
            return array;
        }

        private void ArrayToHeap(Record[] array, int index, int size)
        {
            int largest = index;
            int left = 2 * index + 1;
            int right = 2 * index + 2;
            if (left < size && array[largest].key < array[left].key)
            {
                largest = left;
            }
            if (right < size && array[largest].key < array[right].key)
            {
                largest = right;
            }
            if (largest != index)
            {
                dataGridView.Rows[0].Cells[largest].Style.BackColor = System.Drawing.Color.Red;
                dataGridView.Rows[0].Cells[index].Style.BackColor = System.Drawing.Color.Green;
                dataGridView.Refresh();
                Delay(1000);
                Swap(array, largest, index);
                ArrayToHeap(array, largest, size);
            }
        }

        private void Delay(int timeDelay)
        {
            int i = 0;
            var delayTimer = new System.Timers.Timer();
            delayTimer.Interval = timeDelay;
            delayTimer.AutoReset = false;
            delayTimer.Elapsed += (s, args) => i = 1;
            delayTimer.Start();
            while(i == 0) { };
        }
    }
}
